﻿using Location_API.Model;
using Microsoft.AspNetCore.Mvc;
using System.Xml.Linq;

namespace Location_API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LocationsController : ControllerBase
    {
        /// <summary>
        /// This will give the location availability based on the opening and closing time.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetLocationsWithAvailability()
        {
            var availableLocations = locationsData
                                     .Where(l => l.OpeningTime >= TimeSpan.FromHours(10) && l.ClosingTime <= TimeSpan.FromHours(13))
                                     .ToList();
            return Ok(availableLocations);
        }
        /// <summary>
        /// This is the dummy data for the locations.
        /// </summary>
        private List<Location> locationsData = new List<Location>
        {
            new Location { Id = 1, Name = "Pharmacy A", Type = "Pharmacy", OpeningTime = TimeSpan.FromHours(10), ClosingTime = TimeSpan.FromHours(13) },
            new Location { Id = 2, Name = "Bakery B", Type = "Bakery", OpeningTime = TimeSpan.FromHours(10), ClosingTime = TimeSpan.FromHours(13) },
            new Location { Id = 3, Name = "Barber Shop C", Type = "Barber Shop", OpeningTime = TimeSpan.FromHours(9), ClosingTime = TimeSpan.FromHours(20) },
            new Location { Id = 4, Name = "Supermarket D", Type = "Supermarket", OpeningTime = TimeSpan.FromHours(6), ClosingTime = TimeSpan.FromHours(22) },
            new Location { Id = 5, Name = "Candy Store E", Type = "Candy Store", OpeningTime = TimeSpan.FromHours(10), ClosingTime = TimeSpan.FromHours(17) },
            new Location { Id = 6, Name = "Cinema Complex F", Type = "Cinema Complex", OpeningTime = TimeSpan.FromHours(12), ClosingTime = TimeSpan.FromHours(23) },
            new Location { Id = 7, Name = "Bookstore G", Type = "Bookstore", OpeningTime = TimeSpan.FromHours(8), ClosingTime = TimeSpan.FromHours(20) },
            new Location { Id = 8, Name = "Coffee Shop H", Type = "Coffee Shop", OpeningTime = TimeSpan.FromHours(7), ClosingTime = TimeSpan.FromHours(18) },
            new Location { Id = 9, Name = "Pet Store I", Type = "Pet Store", OpeningTime = TimeSpan.FromHours(10), ClosingTime = TimeSpan.FromHours(19) },
            new Location { Id = 10, Name = "Gym J", Type = "Gym", OpeningTime = TimeSpan.FromHours(6), ClosingTime = TimeSpan.FromHours(21) }
        };
    }
}
